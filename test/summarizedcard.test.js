import React from 'react';
import ReactDOM from 'react-dom';
import { SummarizedItemCard } from '../src/components/SummarizedItemCard/SummarizedItemCard';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<SummarizedItemCard />, div);
});
