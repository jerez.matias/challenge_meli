import React from 'react';
import ReactDOM from 'react-dom';
import { ItemDetailComponent } from '../src/components/ItemDetail/ItemDetailComponent';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ItemDetailComponent />, div);
});
