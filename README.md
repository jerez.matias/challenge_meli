#MELI Challenge

###### Author: Matias L. Jerez

##Installing / Getting started

###The easy way:
#####Dependencies:
- Docker

Promted on the project's directory, run:
```shell
    docker build -t meli/ch_api .
    docker run -p 127.0.0.1:80:3000/tcp meli/ch_api
```
After that point your browser to localhost and let the magic begins

### Path of the ninja:
#####Dependencies:
- Node & NPM
- Alternatively you can use yarn

####Start your development server
Promted on the project's directory
```shell
    npm i
    npm run dev
```
or
```shell
    yarn install
    yarn dev
```
After that point your browser to localhost:3000 to start coding.
Has hot code reloading and you can debug the code with node inspector on default port. 

To start a production ready server just run:
```shell
    yarn build
    yarn start
```

##Some beautiful stuff 
You can check the API documentation on <basepath>/api-doc