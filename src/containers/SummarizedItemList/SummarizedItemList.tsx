import Grid from '@material-ui/core/Grid';
import React, { Component } from 'react';
import './SummarizedItemList.scss';
import {SummarizedItemCard} from '../../components/SummarizedItemCard/SummarizedItemCard';

export class SummarizedItemList extends Component {
    constructor() {
        super();
    }

    ListSummarizedItems(props) {
        const listOfItems = props.items.map((item) => <li key={item.id}><SummarizedItemCard item={item}/></li>);
        return (
            <ul className='ml-unstyled-list'>{listOfItems}</ul>
        );
    }

    render() {
        return (
            <div className='ml-flex-items-list-ctn'>
                <div className='ml-main-ctn'>
                    <this.ListSummarizedItems items={this.props.items} />
                </div>
            </div>
        );
    }
}
