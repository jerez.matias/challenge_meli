import { Component } from 'react';
import { connect } from 'react-redux';
import { Breadcrumb } from '../../components/Breadcrumb/Breadcrumb';
import { ItemDetailComponent } from '../../components/ItemDetail/ItemDetailComponent';
import { SearchBox } from '../../components/SearchBox/SearchBox';

export default connect ((state) => state) (
    ({ data }) => {

        return (
            <div>
                <SearchBox/>
                <div className='ml-flex-container'>
                    <div className='ml-filler-x2' />
                    <div className='ml-main-ctn'>
                        <Breadcrumb path={data.category.categoriesUntilRoot.map((ctgBasic) => ctgBasic.name)}/>
                        <ItemDetailComponent item={data.item}/>
                    </div>
                    <div className='ml-filler-x2' />
                </div>
            </div>
        );
    },
);
