import { connect } from 'react-redux';
import { Breadcrumb } from '../../components/Breadcrumb/Breadcrumb';
import { SearchBox } from '../../components/SearchBox/SearchBox';
import { SummarizedItemList } from '../SummarizedItemList/SummarizedItemList';

export default connect ((state) => state) (
    ({ data }) => {

        return (
            <div>
                <SearchBox/>
                <div className='ml-flex-container'>
                    <div className='ml-filler-x2' />
                    <div className='ml-main-ctn'>
                        <Breadcrumb path={data.topCategory.categoriesUntilRoot.map((ctgBasic) => ctgBasic.name)}/>
                        <SummarizedItemList items={data.items}/>
                    </div>
                    <div className='ml-filler-x2' />
                </div>
            </div>
        );
    },
);
