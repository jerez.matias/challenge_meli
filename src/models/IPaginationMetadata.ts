export interface IPaginationMetadata {
    limit: number;
    offset: number;
    total: number;
}
