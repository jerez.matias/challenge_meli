export interface IPricePLO {
    currency: string;
    amount: number;
    decimals: number;
}