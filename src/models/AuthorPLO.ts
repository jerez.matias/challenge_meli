import { IAuthorPLO } from './IAuthorPLO';

export class AuthorPLO implements IAuthorPLO {
    lastname: string;
    name: string;
    constructor(name: string, lastname: string) {
        this.name = name;
        this.lastname = lastname;
    }

}
