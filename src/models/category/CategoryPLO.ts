import { BasicObject } from '../BasicObject';
import { ICategoryPLO } from './ICategoryPLO';

export class CategoryPLO implements ICategoryPLO {
    id: string;
    name: string;
    categoriesUntilRoot: BasicObject[];

    constructor(id, name, pathToRoot) {
        this.id = id;
        this.name = name;
        this.categoriesUntilRoot = pathToRoot;
    }
}
