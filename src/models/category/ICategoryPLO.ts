import { IBasicObject } from '../IBasicObject';

export interface ICategoryPLO {
    id: string;
    name: string;
    categoriesUntilRoot: IBasicObject[];
}
