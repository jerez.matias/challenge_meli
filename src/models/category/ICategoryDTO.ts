import { IBasicObject } from '../IBasicObject';

export interface ICategoryDTO extends IBasicObject {
    picture: string;
    permalink: string;
    total_items_in_this_category: number;
    path_from_root: IBasicObject[];
    children_categories: any[];
    attributes_types: string;
    settings: any;
    meta_categ_id: number;
    attributable: boolean;
}
