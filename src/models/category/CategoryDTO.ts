import { BasicObject } from '../BasicObject';
import { CategoryPLO } from './CategoryPLO';
import { ICategoryDTO } from './ICategoryDTO';

export class CategoryDTO implements ICategoryDTO {
    attributable: boolean;
    attributes_types: string;
    children_categories: any[];
    id: string;
    meta_categ_id: number;
    name: string;
    path_from_root: BasicObject[];
    permalink: string;
    picture: string;
    settings: any;
    total_items_in_this_category: number;

    transformToPLO(): CategoryPLO {
         return new CategoryPLO(this.id, this.name, this.path_from_root);
    }
}
