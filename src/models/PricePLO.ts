import { IPricePLO } from './IPricePLO';

export class PricePLO implements IPricePLO {
    amount: number;
    currency: string;
    decimals: number;
    constructor(amount, currency, decimals = 0) {
        this.amount = amount;
        this.currency = currency;
        this.decimals = decimals;
    }
}
