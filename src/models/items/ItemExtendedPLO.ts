import { PricePLO } from '../PricePLO';
import { IItemDescriptionDTO } from './IItemDescriptionDTO';
import { IItemDetailDTO } from './IItemDetailDTO';
import { IItemExtendedPLO } from './IItemExtendedPLO';

export class ItemExtendedPLO implements IItemExtendedPLO {
    condition: string;
    description: string;
    free_shipping: boolean;
    id: string;
    picture: string;
    price: PricePLO;
    sold_quantity: number;
    title: string;

    transformFromDTO(detail: IItemDetailDTO, description: IItemDescriptionDTO) {
        this.id = detail.id;
        this.description = description.plain_text;
        this.free_shipping = detail.shipping.free_shipping;
        this.condition = detail.condition;
        this.picture = detail.pictures[0].secure_url;
        this.price = new PricePLO(detail.price, detail.currency_id);
        this.sold_quantity = detail.sold_quantity;
        this.title = detail.title;
    }
}
