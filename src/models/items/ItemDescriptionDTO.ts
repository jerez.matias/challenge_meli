import { IItemDescriptionDTO } from './IItemDescriptionDTO';

export class ItemDescriptionDTO implements IItemDescriptionDTO {
    date_created: string;
    last_updated: string;
    plain_text: string;
    snapshot: any;
    text: string;
}
