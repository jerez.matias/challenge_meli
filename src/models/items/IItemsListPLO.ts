import { ICategoryPLO } from '../category/ICategoryPLO';
import { IAuthorPLO } from '../IAuthorPLO';
import { IPaginationMetadata } from '../IPaginationMetadata';
import { IItemBasicPLO } from './IItemBasicPLO';

export interface IItemsListPLO {
    author: IAuthorPLO;
    categories: string[];
    items: IItemBasicPLO[];
    pagination: IPaginationMetadata;
    topCategory: ICategoryPLO;
}
