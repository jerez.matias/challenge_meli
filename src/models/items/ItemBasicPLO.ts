import { PricePLO } from '../PricePLO';
import { IItemBasicPLO } from './IItemBasicPLO';

export class ItemBasicPLO implements IItemBasicPLO {
    condition: string;
    free_shipping: boolean;
    id: string;
    picture: string;
    price: PricePLO;
    title: string;
    constructor(params) {
        Object.assign(this, params);
    }
}
