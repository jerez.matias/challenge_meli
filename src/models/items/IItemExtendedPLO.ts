import { IItemBasicPLO } from './IItemBasicPLO';

export interface IItemExtendedPLO extends IItemBasicPLO {
    sold_quantity: number;
    description: string;
}
