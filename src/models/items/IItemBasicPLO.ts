import {IPricePLO} from "../IPricePLO";

export interface IItemBasicPLO {
    id: string;
    title: string;
    price: IPricePLO;
    picture: string;
    condition: string;
    free_shipping: boolean;
}