import { AuthorPLO } from '../AuthorPLO';
import { CategoryPLO } from '../category/CategoryPLO';
import { PaginationMetadata } from '../PaginationMetadata';
import { IItemsListPLO } from './IItemsListPLO';
import { ItemBasicPLO } from './ItemBasicPLO';

export class ItemsListPLO implements IItemsListPLO {
    author: AuthorPLO;
    categories: string[];
    items: ItemBasicPLO[];
    pagination: PaginationMetadata;
    topCategory: CategoryPLO;

    constructor(author, categories, topCategory, items, pagination) {
        this.author = author;
        this.categories = categories;
        this.items = items;
        this.pagination = pagination;
        this.topCategory = topCategory;
    }
}
