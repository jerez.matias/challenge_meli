import { ICategoryPLO } from '../category/ICategoryPLO';
import { IAuthorPLO } from '../IAuthorPLO';
import { IItemExtendedPLO } from './IItemExtendedPLO';

export interface IItemDetailPLO {
    author: IAuthorPLO;
    category: ICategoryPLO;
    item: IItemExtendedPLO;
}
