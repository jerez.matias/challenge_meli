import { AuthorPLO } from '../AuthorPLO';
import { CategoryPLO } from '../category/CategoryPLO';
import { IItemDetailPLO } from './IItemDetailPLO';
import { ItemExtendedPLO } from './ItemExtendedPLO';

export class ItemDetailPLO implements IItemDetailPLO {
    author: AuthorPLO;
    category: CategoryPLO;
    item: ItemExtendedPLO;

    constructor(author, category, item) {
        this.author = author;
        this.category = category;
        this.item = item;
    }
}
