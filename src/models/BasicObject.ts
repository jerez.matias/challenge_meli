import { IBasicObject } from './IBasicObject';

export class BasicObject implements IBasicObject {
    id: string;
    name: string;
}
