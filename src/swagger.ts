export const swagger = {
  swagger: '2.0',
  info: {
    version: '1.0.0',
    title: 'Meli Challenge',
    description: 'A simple decorator api to serve items search, detail and server rendered views',
  },
  schemes: [
    'http',
  ],
  host: 'localhost:3000',
  basePath: '/',
  paths: {
    '/': {
      get: {
        summary: 'Gets main page SSR',
        description: 'Returns a searchbox to start searching',
        responses: {
          200: {
            description: 'Serve a view to start searching',
          },
        },
      },
    },
    '/health': {
      get: {
        summary: 'API Healthcheck',
        description: 'Returns uptime of the SSR server',
        responses: {
          200: {
            description: 'Show server\'s uptime',
          },
        },
      },
    },
    '/items': {
      get: {
        summary: 'Gets some items filtered by query for SSR',
        description: 'Returns a list rendered on the server containing items, pagination, categories, top and author.',
        parameters: [
          {
            name: 'search',
            in: 'query',
            description: 'Search text',
            type: 'string',
          },
          {
            name: 'limit',
            in: 'query',
            description: 'Results per page',
            type: 'integer',
          },
        ],
        responses: {
          200: {
            description: 'A list of items rendered on the server filtered by the query parameters',
            schema: {
              $ref: '#/definitions/ItemList',
            },
          },
        },
      },
    },
    '/_data/items': {
      get: {
        summary: 'Gets some items filtered by query for JSON',
        description: 'Returns JSON containing items, pagination, categories, top and author.',
        parameters: [
          {
            name: 'search',
            in: 'query',
            description: 'Search text',
            type: 'string',
          },
          {
            name: 'limit',
            in: 'query',
            description: 'Results per page',
            type: 'integer',
          },
        ],
        responses: {
          200: {
            description: 'A JSON list of items filtered by the query parameters',
            schema: {
              $ref: '#/definitions/ItemList',
            },
          },
        },
      },
    },
    '/items/:id': {
      get: {
        summary: 'Gets detailed description of an item for SSR',
        description: 'Returns a detailed view rendered on the server of an item',
        parameters: [
          {
            name: 'id',
            in: 'parameter',
            description: 'Id of the item to search',
            type: 'string',
          },
        ],
        responses: {
          200: {
            description: 'A server rendered detailed view of an item',
            schema: {
              $ref: '#/definitions/ItemDetailed',
            },
          },
        },
      },
    },
    '/_data/items/:id': {
      get: {
        summary: 'Gets detailed description of an item for JSON',
        description: 'Returns a detailed JSON of an item',
        parameters: [
          {
            name: 'id',
            in: 'parameter',
            description: 'Id of the item to search',
            type: 'string',
          },
        ],
        responses: {
          200: {
            description: 'A detailed JSON of an item',
            schema: {
              $ref: '#/definitions/ItemDetailed',
            },
          },
        },
      },
    },
  },
  definitions: {
    Author: {
      properties: {
        lastName: {
          type: 'string',
        },
        name: {
          type: 'string',
        },
      },
    },
    Item: {
      properties: {
        condition: {
          type: 'string',
        },
        free_shipping: {
          type: 'boolean',
        },
        id: {
          type: 'string',
        },
        picture: {
          type: 'string',
        },
        price: {
          type: 'number',
        },
        title: {
          type: 'string',
        },
      },
    },
    ItemExtended: {
      properties: {
        condition: {
          type: 'string',
        },
        free_shipping: {
          type: 'boolean',
        },
        id: {
          type: 'string',
        },
        picture: {
          type: 'string',
        },
        price: {
          type: 'number',
        },
        title: {
          type: 'string',
        },
        sold_quantity: {
          type: 'number',
        },
        description: {
          type: 'string',
        },
      },
    },
    Pagination: {
      properties: {
        limit: {
          type: 'number',
        },
        offset: {
          type: 'number',
        },
        total: {
          type: 'number',
        },
      },
    },
    ItemList: {
      properties: {
        author: {
          $ref: '#/definitions/Author',
        },
        categories: {
          type: 'array',
          items: {
            type: 'string',
          },
        },
        items: {
          type: 'array',
          items: {
            $ref: '#/definitions/Item',
          },
        },
        pagination: {
          $ref: '#/definitions/Pagination',
        },
        topCategory: {
          $ref: '#/definitions/Category',
        },
      },
    },
    Category: {
      properties: {
        id: {
          type: 'string',
        },
        name: {
          type: 'string',
        },
        categoriesUntilRoot: {
          type: 'array',
          items: {
            $ref: '#/definitions/BasicObject',
          },
        },
      },
    },
    BasicObject: {
      properties: {
        id: {
          type: 'string',
        },
        name: {
          type: 'string',
        },
      },
    },
    ItemDetailed: {
      properties: {
        author: {
          $ref: '#/definitions/Author',
        },
        item: {
          $ref: '#/definitions/ItemExtended',
        },
        category: {
          $ref: '#/definitions/Category',
        },
      },
    },
  },
};
