import React, { Component } from 'react';
import { connect, Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import ItemDetailContainer from '../containers/ItemDetailContainer/ItemDetailContainer';
import { getItems } from '../store/actions/item';

class ItemDetail extends Component {
    static async getInitialProps({req, query}) {
        // DISPATCH Actions from reduxStore.dispatch
        const isServer = !!req;
        if (isServer) {
            return {item: query.itemData};
        } else {
            const res = await fetch(`/_data/items/`, {
                headers: {Accept: 'application/json'},
            });
            const json = await res.json();
            return {item: json};
        }
    }

    componentDidMount() {
        // DISPATCH Actions from mapDispatchToProps
    }

    componentWillMount() {

    }

    componentWillUnmount() {

    }

    render() {
        return <ItemDetailContainer data={this.props.item}/>;
    }

}

const mapDispatchToProps = (dispatch) => {
    return {
        getItems: bindActionCreators(getItems, dispatch),
    };
};
export default connect(
    null,
    mapDispatchToProps,
)(ItemDetail);
