import Router from 'next/router';
import React, { Component } from 'react';
import { connect, Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import Home from '../containers/Home/Home';
import { fetchingItems, getItems } from '../store/actions/item';

class Index extends Component {
    static async getInitialProps({ req, query }) {
        // DISPATCH Actions from reduxStore.dispatch
        const isServer = !!req;
        if (isServer) {
            return {item: query.itemsData};
        } else {
            const res = await fetch(`/_data/items?search=${query.search}`, {
                headers: {Accept: 'application/json'},
            });
            const json = await res.json();
            return {item: json};
        }
    }

    componentDidMount() {
        // DISPATCH Actions from mapDispatchToProps

    }

    componentWillMount() {

    }

    componentWillUnmount() {

    }

    render() {
        return <Home />;
    }

}

const mapDispatchToProps = (dispatch) => {
    return {
        getItems: bindActionCreators(getItems, dispatch),
    };
};
export default connect(
    null,
    mapDispatchToProps,
)(Index);
