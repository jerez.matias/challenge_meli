import * as envalid from 'envalid';

export const env = envalid.cleanEnv(process.env, {
  NODE_ENV: envalid.str({ default: 'development' }),
  PORT: envalid.port({ default: 3000 }),
});
