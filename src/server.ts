import * as compression from 'compression';
import * as cors from 'cors';
import * as express from 'express';
import * as healthcheck from 'express-healthcheck';
import * as morgan from 'morgan';
import * as next from 'next';
import * as nextI18NextMiddleware from 'next-i18next/middleware';
import * as pathMatch from 'path-match';
import * as swaggerUI from 'swagger-ui-express';

import { join } from 'path';
import { format, parse } from 'url';
import api from './api';

// const env = process.env.NODE_ENV;
import { ItemsService } from './api/items/items.service';
import { env } from './config';
import { nextI18next } from './i18n';
import { swagger } from './swagger';

const app = next({
    dir: __dirname,
    conf:
        env.NODE_ENV === 'production'
            ? { distDir: '../.next', poweredByHeader: false }
            : undefined,
    dev: env.NODE_ENV !== 'production',
});
const handle = app.getRequestHandler();

const rootStaticFiles = [
    '/browserconfig.xml',
    '/favicon.ico',
    '/manifest.json',
    '/robots.txt',
];

(async () => {
    try {

        await app.prepare();
        const server: express.Application = express();

        // PathMatchDeclaration
        const routeMatch = pathMatch();

        server.use(nextI18NextMiddleware(nextI18next));

        server.use(compression());
        server.disable('x-powered-by');
        // logs to console minimal information
        // :method :url :statusCode :time :content length
        if (env.NODE_ENV === 'development' || env.NODE_ENV === 'local') {
            server.use(morgan('dev'));
        }
        server.use(cors());
        // api routes
        server.use('/api', api);

        server.use('/api-doc', swaggerUI.serve, swaggerUI.setup(swagger));

        // Add healthcheck endpoint
        server.get('/health', healthcheck());

        server.get('/items', async (req, res) => {
            if (!req.query.search) res.redirect('/');
            if (!req.query.limit) req.query.limit = 4;
            const query = {...req.query, q: req.query.search};
            const itemsData = await ItemsService.getAll(query, req.headers);
            app.render(req, res, '/items', { itemsData });
        });

        server.get('/_data/items', async (req, res) => {
            if (!req.query.search) res.redirect('/');
            if (!req.query.limit) req.query.limit = 4;
            const query = {q: req.query.search};
            const itemsData = await ItemsService.getAll(query, req.headers);
            res.json(itemsData);
        });

        // handle any other requests
        server.get('*', async (req, res) => {
            const { pathname, query } = parse(req.url, true);

            if (rootStaticFiles.indexOf(pathname) > -1) {
                const path = join(__dirname, 'static', pathname);
                return app.serveStatic(req, res, path);
            }

            const itemDataDetailPath = routeMatch('/_data/items/:id');
            const itemDataDetailPathParams = itemDataDetailPath(pathname);
            if (itemDataDetailPathParams) {
                const itemsData = await ItemsService.getDetail(itemDataDetailPathParams.id, req.headers);
                res.json(itemsData);
            }

            const itemDetailPath = routeMatch('/items/:id');
            const itemDetailPathParams = itemDetailPath(pathname);
            if (itemDetailPathParams) {
                const itemData = await ItemsService.getDetail(itemDetailPathParams.id, req.headers);
                app.render(req, res, '/itemDetail', {itemData});
            }

            if (
                pathname.length > 1 &&
                pathname.slice(-1) === '/' &&
                pathname.indexOf('/_next/') !== 0
            ) {
                return res.redirect(
                    format({
                        pathname: pathname.slice(0, -1),
                        query,
                    }),
                );
            }

            return handle(req, res);
        });

        server.listen(env.PORT, (err) => {
            if (err) {
                throw err;
            }
            console.log(`Server ready and running on port: ${env.PORT} & env ${env.NODE_ENV}`);
        });
    } catch (err) {
        console.error(err.message);
    }

})();
