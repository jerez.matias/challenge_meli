/* eslint-disable @typescript-eslint/no-var-requires */

import i18n from 'i18next';
import * as ICU from 'i18next-icu';
import en from 'i18next-icu/locale-data/en';
import es from 'i18next-icu/locale-data/es';
import { NextFunctionComponent } from 'next';
import NextI18next from 'next-i18next';

const use: any[] = [];
const icu = new ICU({});
icu.addLocaleData(es);
icu.addLocaleData(en);
use.push(icu);

let detectionOrder: string[] = [];
if (typeof window === 'undefined') {
    const { env } = require('./config');
    const i18nextMiddleware = require('i18next-express-middleware');
    const languageDetector = new i18nextMiddleware.LanguageDetector(null, {
        order: ['enforcedLocale', 'languageByDomain'],
    });

    languageDetector.addDetector({
        name: 'enforcedLocale',
        lookup: () => env.ENFORCED_LOCALE,
        cacheUserLanguage: () => {
            /**/
        },
    });

    languageDetector.addDetector({
        name: 'languageByDomain',
        lookup: (opts) => {
            const hostWithoutPort = (opts.headers.host || '').replace(/\:\d+$/, '');
            return hostWithoutPort === env.HOST_RU ? 'es' : 'en';
        },
        cacheUserLanguage: () => {
            /**/
        },
    });
    use.push(languageDetector);
    detectionOrder = ['enforcedLocale', 'languageByDomain'];
}

export const nextI18next = new NextI18next({
    defaultLanguage: 'es',
    fallbackLng: 'es',
    defaultNS: 'common',
    otherLanguages: ['en'],
    localePath: './locales',
    keySeparator: '###',
    use,
    browserLanguageDetection: false,
    detection: {
        order: detectionOrder,
    },
} as any);

export const includeDefaultNamespaces = (namespaces: string[]) =>
    ['common', '_error'].concat(namespaces);

export const appWithTranslation = nextI18next.appWithTranslation;
// export const Trans = nextI18next.Trans as typeof OriginalTrans;
export const withNamespaces = nextI18next.withNamespaces; // as typeof originalWithNamespaces;
export type TFunction = i18n.TFunction;
export type I18n = i18n.i18n;
