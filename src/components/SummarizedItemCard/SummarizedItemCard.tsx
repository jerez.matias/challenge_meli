import Router from 'next/router';
import React, { Component } from 'react';
import './SummarizedItemCard.scss';

export class SummarizedItemCard extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
    }

    handleGoDetail = () => {
        const href = `/items/${this.props.item.id}`;
        Router.push(href);
    }

    render() {
        return (
            <div onClick={this.handleGoDetail} className='ml-summarized-item-card-ctn'>
                <div className='ml-thumb-ctn'>
                    <img
                        className='ml-thumb-image'
                        alt={this.props.item.title}
                        src={this.props.item.thumbnail}
                    />
                </div>
                <div className='ml-info-ctn'>
                    <div className='ml-price-ctn'>
                        <div className='ml-price-text'>
                            {
                                new Intl.NumberFormat('es-AR', {
                                style: 'currency',
                                currency: 'ARS',
                                minimumFractionDigits: 0,
                                maximumFractionDigits: 0,
                                }).format(this.props.item.price)
                            }
                        </div>
                        { !this.props.item.shipping.free_shipping && (<div className='ml-free-shipping-ctn'>
                            <img
                                alt='free-shipping'
                                src='/static/images/ic_shipping@2x.png'
                            />
                        </div>)}
                    </div>
                    <div className='ml-title-ctn'>{this.props.item.title}</div>
                </div>
                <div className='ml-region-ctn'>{this.props.item.address.state_name}</div>
            </div>
        );
    }
}
