import Router from 'next/router';
import React, { Component } from 'react';
import './SearchBox.scss';

export class SearchBox extends Component {

    constructor(props) {
        super(props);
        this.state = { value: '' };
    }

    handleSubmit = (event) => {
        const href = `/items/?search=${this.state.value}`;
        Router.push(href);
    }

    handleChange = (event) => {
        this.setState({value: event.target.value});
    }

    render() {
        return (
            (
                <div className='ml-header'>
                    <div className='ml-filler'/>
                    <div className='ml-logo-ctn'>
                        <img
                            alt='Meli Logo'
                            src='/static/images/Logo_ML.png'
                        />
                    </div>
                    <div className='ml-input-ctn'>
                        <form noValidate={true} onSubmit={((event) => event.preventDefault())}>
                            <input
                                className='ml-input-box'
                                type='text'
                                value={this.state.value}
                                placeholder='Nunca dejes de buscar'
                                onChange={this.handleChange}
                            />
                            <button className='ml-find-icon-btn' onClick={this.handleSubmit}>
                                <img
                                    alt='Encontrar'
                                    src='/static/images/ic_Search.png'
                                />
                            </button>
                        </form>
                    </div>
                    <div className='ml-filler' />
                </div>
            )
        );
    }
}
