import Grid from '@material-ui/core/Grid';
import React, { Component } from 'react';
import './Breadcrumb.scss';

export class Breadcrumb extends Component {
    constructor(props: any) {
        super(props);
    }

    CreateBreadcrumb(props) {
        const breadcrumbPath = props.path.map((path) => <li className='ml-breadcrumb-item' key={path}> {path} </li>);
        return (
            <ul className='ml-unstyled-list'>{breadcrumbPath}</ul>
        );
    }

    render() {
        return (

                <div className='ml-breadcrumb-ctn'>
                    <this.CreateBreadcrumb path={this.props.path} />
                </div>

        );
    }
}
