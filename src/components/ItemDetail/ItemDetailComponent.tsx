import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import React, { Component } from 'react';
import './ItemDetailComponent.scss';

export class ItemDetailComponent extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
    }

    render() {
        return (

                    <div className='ml-detail-item-ctn'>
                        <div className='ml-info-ctn'>
                            <div className='ml-thumb-ctn'>
                                <img
                                    className='ml-thumb-image'
                                    alt='Picture'
                                    src={this.props.item.picture}
                                />
                            </div>
                            <div className='ml-description-ctn'>
                                <h1 className='ml-title-desciption'>Descripción del producto</h1>
                                <p>{this.props.item.description}</p>
                            </div>
                        </div>
                        <div className='ml-price-ctn'>
                            <p className='ml-status-sell-count'>
                                <span>{this.props.item.condition === 'used' ? 'usado' : 'nuevo'}</span> - <span>{this.props.item.sold_quantity} vendidos</span>
                            </p>
                            <p className='ml-detail-title'>{this.props.item.title}</p>
                            <h1 className='ml-price-text'>
                                {
                                    new Intl.NumberFormat('es-AR', {
                                        style: 'currency',
                                        currency: this.props.item.price.currency,
                                        minimumFractionDigits: 0,
                                        maximumFractionDigits: 0,
                                    }).format(this.props.item.price.amount)
                                }
                                {!this.props.item.free_shipping && (<span className='ml-free-shipping-ctn'>
                                    <img
                                        alt='free-shipping'
                                        src='/static/images/ic_shipping@2x.png'
                                    />
                                </span>)}
                            </h1>
                            <button className='ml-buy-button-ml'>Comprar</button>
                        </div>
                    </div>

        );
    }
}
