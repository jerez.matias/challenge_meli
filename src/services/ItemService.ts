export class ItemService {
    static async getItems(query, page, size) {
        if(!query) query = 'ipod';
        const response = await fetch(
            `/api/items?q=${query}&page=${page}&size=${size}`,
        );
        return response.json();
    }
}
