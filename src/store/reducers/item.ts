import * as itemActions from '../actions/item';

const initialState = {
    loading: false,
    items: [],
    pagination: {
        page: 0,
        totalPages: 0,
    },
};

export default (state = initialState, action) => {
    switch (action.type) {
        case itemActions.FETCHING_ITEMS:
            return {...state, loading: true};
        case itemActions.FETCHING_ITEMS_SUCCESS:
            return {...state,
                    loading: false,
                    items: action.payload.content,
                    pagination: {
                    page: action.payload.number + 1,
                    totalPages: action.payload.totalPages,
                }};
        default:
            return state;
    }
};
