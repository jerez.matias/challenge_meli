import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducers';

export default function configureStore(initialState = {}) {
    const middlewares = [
        thunk,
    ];

    const enhancers = [applyMiddleware(...middlewares)];

    return createStore(reducer, initialState, compose(...enhancers));
}
