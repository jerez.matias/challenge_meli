import { ItemService } from '../../services/ItemService';

export const FETCHING_ITEMS = '[Items] Fetching All Started';
export const fetchingItems = (isServer, query) => ({
    type: FETCHING_ITEMS,
});

export const FETCHING_ITEMS_SUCCESS = '[Items] Fetching Success';
export const itemFetchedSuccess = (payload) => ({
    type: FETCHING_ITEMS_SUCCESS,
    payload,
});

export const FETCHING_ITEMS_ERROR = '[Items] Fetching Error';
export const itemFetchedError = (error) => ({
    type: FETCHING_ITEMS_ERROR,
    payload: error,
});

export const getItems = (isServer, query = '', page = 0, size = 4) => {
    return async (dispatch) => {
        dispatch(fetchingItems(isServer, query));

        try {
            const items = await ItemService.getItems(query, page, size);
            console.log(items);
            dispatch(itemFetchedSuccess(items));
        } catch (err) {
            dispatch(itemFetchedError(err));
        }
    }
};
