import 'reflect-metadata';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as httpStatus from 'http-status';
import * as cookieParser from 'cookie-parser';
import { fourOFour } from '../middlewares/404-middleware';
import { errorConverter } from '../core/errors/error-converter-middleware';
import ItemsRouter from './items/items.route';

class Api {

  public api: express.Application;

  constructor() {
    this.api = express();
    this.middleware();
  }

  private middleware(): void {
    this.api.use(bodyParser.json());
    this.api.use(bodyParser.urlencoded({extended: false}));
    this.api.use(cookieParser());
    this.api.use('/items', ItemsRouter);

    // try to convert all error to common interface
    this.api.use(errorConverter);

    this.api.use((err, req, res, next) => {
      // log the error and the request as it is.
      const error = {
        message: err.isPublic
          ? err.message
          : httpStatus[err.status] || httpStatus[httpStatus.INTERNAL_SERVER_ERROR],
        stack: err.isPublic ? err.stack : undefined,
        code: err.code,
        response: err.response,
      };
      if (this.api.get('env') === 'production')
        Reflect.deleteProperty(error, 'stack');

      res.status(err.status || httpStatus.INTERNAL_SERVER_ERROR).json(error);
      console.error(err);
    });

    this.api.use(fourOFour);
    this.api.use((req, res) => {
      if (res.statusCode === httpStatus.NOT_FOUND)
        res.json({message: httpStatus[httpStatus.NOT_FOUND]});
    });
  }

}

export default new Api().api;
