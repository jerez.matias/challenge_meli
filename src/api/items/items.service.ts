import axios from 'axios';
import * as _ from 'lodash';
import { AuthorPLO } from '../../models/AuthorPLO';
import { CategoryDTO } from '../../models/category/CategoryDTO';
import { ItemBasicPLO } from '../../models/items/ItemBasicPLO';
import { ItemDetailPLO } from '../../models/items/ItemDetailPLO';
import { ItemExtendedPLO } from '../../models/items/ItemExtendedPLO';
import { ItemsListPLO } from '../../models/items/ItemsListPLO';
import { PaginationMetadata } from '../../models/PaginationMetadata';
import { CategoriesService } from '../categories/categories.service';

interface ICategoryCount {
  id: string;
  quantity: number;
}

export class ItemsService {

  public static async getAll(query, headers): Promise<any> {
    if (!query.limit) query.limit = 4;
    const result = await axios
      .get('https://api.mercadolibre.com/sites/MLA/search',
          { params: query});

    const author = new AuthorPLO('Matias', 'Jerez');
    const {total, offset, limit} = result.data.paging;
    const pagination: PaginationMetadata = new PaginationMetadata(limit, offset, total);
    const items: ItemBasicPLO[] = [];
    const categoryObject = {};
    const categoryHitsArray: ICategoryCount[] = [];

    result.data.results.map((result) => {
        items.push(new ItemBasicPLO(result));
        const categoryHit: ICategoryCount = { id: result.category_id, quantity: 1 };
        const indexOfCategory = _.findIndex(categoryHitsArray, (ctgHit) => ctgHit.id === result.category_id);
        let replaceFlag = 0, positionToAdd = categoryHitsArray.length;
        if (indexOfCategory >= 0) {
            categoryHit.quantity = categoryHitsArray[indexOfCategory].quantity + 1;
            replaceFlag = 1;
            positionToAdd = indexOfCategory;
        }
        categoryHitsArray.splice(positionToAdd, replaceFlag, categoryHit);
    });

    const categoryTop = _.maxBy(categoryHitsArray, (ctgHit) => ctgHit.quantity);
    const arrayOfPromises = [];
    categoryHitsArray.map((category) => {
      arrayOfPromises.push(axios.get(`https://api.mercadolibre.com/categories/${category.id}`));
    });
    const categoriesResponse = await Promise.all(arrayOfPromises);
    const categories: string[] = categoriesResponse.map((categoryRes) => categoryRes.data.name);
    const topCategory: CategoryDTO = Object.assign(
        new CategoryDTO(),
        categoriesResponse.find((ctgry) => ctgry.data.id === categoryTop.id)['data'],
    );
    return new ItemsListPLO(author, categories, topCategory.transformToPLO(), items, pagination);
  }

  public static async getDetail(id: string, headers): Promise<any> {
    const itemDetailPromise = axios.get(`https://api.mercadolibre.com/items/${id}`);
    const itemDescriptionPromise = axios.get(`https://api.mercadolibre.com/items/${id}/description`);

    const [itemDetailResponse, itemDescriptionResponse] =
        await Promise.all([itemDetailPromise, itemDescriptionPromise]);
    const itemDetailExtended: ItemExtendedPLO = new ItemExtendedPLO();
    itemDetailExtended.transformFromDTO(itemDetailResponse.data, itemDescriptionResponse.data);
    const itemCategory = await CategoriesService.getDetail(itemDetailResponse.data.category_id);
    return new ItemDetailPLO(new AuthorPLO('Matias', 'Jerez'), itemCategory, itemDetailExtended);
  }

}
