import { NextFunction, Request, Response } from 'express';
import { ItemsService } from './items.service';

export class ItemsController {

  public static async getAll(request: Request, response: Response, next: NextFunction) {
    try {
      const answer = await ItemsService.getAll(request.query, request.headers);
      response.json(answer);
    } catch (err) {
      next(err);
    }
  }

  public static async getDetail(request: Request, response: Response, next: NextFunction) {
    try {
      const answer = await ItemsService.getDetail(request.params.id, request.headers);
      response.json(answer);
    } catch (err) {
      next(err);
    }
  }

}
