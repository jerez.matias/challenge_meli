import { Router } from 'express';
import { ItemsController } from './items.controller';

const ItemsRouter: Router = Router();

ItemsRouter.route('/:id')
    .get(ItemsController.getDetail);

ItemsRouter.route('/')
    .get(ItemsController.getAll);

export default ItemsRouter;
