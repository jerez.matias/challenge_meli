import axios from 'axios';
import { CategoryDTO } from '../../models/category/CategoryDTO';
import { ICategoryPLO } from '../../models/category/ICategoryPLO';

export class CategoriesService {
    public static async getDetail(id: string): Promise<ICategoryPLO> {
        const categoriesResponse = await axios.get(`https://api.mercadolibre.com/categories/${id}`);

        const categoryDetail: CategoryDTO = Object.assign(
            new CategoryDTO(),
            categoriesResponse['data'],
        );

        return categoryDetail.transformToPLO();
    }
}
